package com.mongo.model;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document
public class MyModel {
	
	
	@Id
	private String id;
	
	@NotNull(message = "todo is should not be null")
	private String todo;
	@NotNull(message = "description is should not be null")
	private String description;
	private boolean completed;
	private Date created;
	private Date updated;

}
