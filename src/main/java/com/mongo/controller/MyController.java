package com.mongo.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mongo.model.MyModel;
import com.mongo.repository.MyRepository;

@RestController
public class MyController {

	
	
	@Autowired
	ModelMapper mapper;
	
	@Autowired
	private MyRepository repo;

	@GetMapping("/")
	public ResponseEntity<?> getAll() {
		List<MyModel> models = repo.findAll();
		if (models.size() > 0) {
			return new ResponseEntity<List<MyModel>>(models, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("No models are available", HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/save")
	public ResponseEntity<?> save(@RequestBody MyModel model) {
		try {
			model.setCreated(new Date(System.currentTimeMillis()));
			repo.save(model);

			return new ResponseEntity<MyModel>(model, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getById(@PathVariable("id") String id) {
		Optional<MyModel> model = repo.findById(id);
		if (model.isPresent()) {
			return new ResponseEntity<>(model, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("No models are available", HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> saveById(@PathVariable("id") String id, @RequestBody MyModel m) {
	

		Optional<MyModel> model = repo.findById(id);
		
		if (model.isPresent()) {
			MyModel myModel=model.get();
			MyModel my = mapper.map(m,myModel.getClass());
			my.setId(id);
			repo.save(my);
			return new ResponseEntity<>(my, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("No models are available", HttpStatus.NOT_FOUND);
		}

	}
	
	@DeleteMapping("/{id}")
	public String deleteById(@PathVariable("id") String id) {
		try {
			repo.deleteById(id);
			return "deleted data of id :"+id;
		} catch (Exception e) {
			return "No model available with the id : " +id;
		}
	}
}


