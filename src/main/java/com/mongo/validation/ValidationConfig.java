package com.mongo.validation;

import javax.validation.Validator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.mapping.event.ValidatingMongoEventListener;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
public class ValidationConfig {
	
	@Bean
	public ValidatingMongoEventListener vmel() {
		return new ValidatingMongoEventListener(lmel());
	}

	@Bean
	private LocalValidatorFactoryBean lmel() {
		return new LocalValidatorFactoryBean();
	}
}
